// requires...
const fs = require('fs');
const path = require('path');
require('dotenv').config

// constants...
const FILE_PATH = path.join(__dirname, '/files');
const ext = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

function createFile(req, res, next) {
  const { filename, content, password } = req.body;
  if (!filename) {
    return res
      .status(400)
      .send({ message: "Please specify valid 'filename' parameter" });
  }

  if (!content) {
    return res
      .status(400)
      .send({ message: "Please specify valid 'content' parameter" });
  }

  const fileExtension = path.extname(filename);
  if (!fileExtension) {
    return res.status(400).send({ message: 'Please write correct filename' });
  }

  if (!ext.includes(fileExtension)) {
    return res.status(400).send({
      message:
        "Please use filename with one of these extension: 'log', 'txt', 'json', 'yaml', 'xml', 'js'"
    });
  }

  if (password) {
    const filePath = path.join(__dirname, '.env');
    const content = `${filename.split('.').join('_')}_PASSWORD=${password}`
    fs.writeFileSync(filePath, `\n${content}`, {flag: 'a'});
  }

  const fullFilePath = path.join(FILE_PATH, filename);
  fs.writeFileSync(fullFilePath, JSON.stringify(content));
  res.status(200).send({ message: 'File created successfully' });
}

function getFiles (req, res, next) {
  const files = fs.readdirSync(FILE_PATH);

  if (!files) {
    return res.status(400).send({ message: 'Client error' });
  }
  res.status(200).send({"message": "Success", "files": files});
}


function getFile (req, res, next) {

  const { filename } = req.params;
  const { password } = req.query;
  const filePath = path.join(FILE_PATH, filename);

  const name = `${filename.split('.').join('_')}_PASSWORD`
  const pass = process.env[name];

  if (!fs.existsSync(filePath)) {
    return res.status(400).send({"message": `No file with '${filename}' filename found`})
  }

  if (pass) {
    if (!password) {
      return res.status(400).send({"message": "This file is password protected. Please write the correct password"})
    }
    if (pass !== password) {
      return res.status(400).send({"message": "Invalid password"})
    }
  }
  
  const extension = path.extname(filename).slice(1);
  const content = fs.readFileSync(filePath, 'utf-8');
  const date = fs.statSync(filePath).birthtime;

  res.status(200).send({
    "message": "Success",
    "filename": filename,
    "content": JSON.parse(content),
    "extension": extension,
    "uploadedDate": date
  })
}

function deleteFile (req, res, next) {
  const { filename } = req.params;
  const { password } = req.query;
  const filePath = path.join(FILE_PATH, filename);
  const name = `${filename.split('.').join('_')}_PASSWORD`
  const pass = process.env[name];

  if (!fs.existsSync(filePath)) {
    return res.status(400).send({"message": `No file with ${filename} filename found`})
  }

  if (pass) {
    if (!password) {
      return res.status(400).send({"message": "This file is password protected. Please write the correct password"})
    }
    if (pass !== password) {
      return res.status(400).send({"message": "Invalid password"})
    }
  }

  fs.unlinkSync(filePath);

  res.status(200).send({
    "message": `File ${filename} was deleted`,
    });
}

function editFile (req, res, next) {
  const { filename } = req.params;
  const { password } = req.query;
  const filePath = path.join(FILE_PATH, filename);
  const { content } = req.body;
  const name = `${filename.split('.').join('_')}_PASSWORD`
  const pass = process.env[name];
  
  if (!fs.existsSync(filePath)) {
    return res.status(400).send({"message": `No file with ${filename} filename found`})
  }

  if (pass) {
    if (!password) {
      return res.status(400).send({"message": "This file is password protected. Please write the correct password"})
    }
    if (pass !== password) {
      return res.status(400).send({"message": "Invalid password"})
    }
  }
  
  if (!content) {
    return res.status(400).send({"message": "Please add new content"})
  }

  fs.writeFileSync(filePath, JSON.stringify(content));
  
  res.status(200).send({
    "message": "Success",
    "filename": filename,
    "newContent": content,
  })
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile
}
